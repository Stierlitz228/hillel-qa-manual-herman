package Lesson_28_Package;

public class Sportcar extends Cars {
    public Sportcar(String brand, String model, double fuelConsumption, double volumeOfTheTank, double maxSpeed, int enginePower, double price) {
        super(brand, model, fuelConsumption, volumeOfTheTank);
        this.maxSpeed = maxSpeed;
        this.enginePower = enginePower;
        this.price = price;
    }

    public double howFastWillOvercomeTheDistance(double distance){            // метод считает расстояние в часах.
        double resultInHour = distance/maxSpeed;
        return resultInHour;
    }
}

