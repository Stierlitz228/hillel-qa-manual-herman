package Lesson_28_Package;

public class Truck extends Cars{
    double carryingCapacity;

    public Truck (String brand, String model, double fuelConsumption, double volumeOfTheTank, double carryingCapacity) {
        super(brand, model, fuelConsumption, volumeOfTheTank);
        this.carryingCapacity = carryingCapacity;
    }

    public String howMuchLoaded(double load){           //считает перегруз.
        String message = "Вес в норме, можно ехать";
        if(load >= carryingCapacity){
            message = "Машина перегружена! Движение невозможно.";
        }

        return message;
    }
}
