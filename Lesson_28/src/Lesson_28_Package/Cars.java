package Lesson_28_Package;

public class Cars {
    public String brand;
    public String model;
    public double maxSpeed;
    public int enginePower;
    public double price;
    public double powerReserve;
    double volumeOfTheTank;
    double fuelConsumption;

    public Cars(String brand, String model, double fuelConsumption, double volumeOfTheTank){
        this.brand = brand;
        this.model = model;
        this.fuelConsumption = fuelConsumption;
        this.volumeOfTheTank = volumeOfTheTank;
    }

    public String speed(int km){
        String message = "Base";
        if (km >= 140){
            message ="Превышен лимит";
        }
        else if (km >= 120){
            message ="Ты уже гонишь";
        }
        else if(km >= 100){
            message = "Нормальная скорость";
        }
        else if(km < 100){
            message = "Безопасная скорость";
        }

        return message;
    }

    public double kmTransferToMile(double km){
        double mile = km * 0.621371;
        return mile;
    }

    public double mileTransferToKM(double mile){
        double km = mile * 1.60934;
        return km;
    }

    public String howFarYouCanDrive(double howFarYouWantDrive){             //указываем как далеко нам нужно ехать и метод
        double calculate = volumeOfTheTank / fuelConsumption;               // считает доедем ли мы согласно расходу топлива и обьема бака.
        String message = "Вы ещё не выехали";
        if(calculate >= howFarYouWantDrive){
            message = "Вы доехали успешно!";
        }
        else {
            message = "У Вас закончился бензин.";
        }

        return message;
    }
}
