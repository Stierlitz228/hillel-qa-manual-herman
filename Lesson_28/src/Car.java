import Lesson_28_Package.Cars;
import Lesson_28_Package.Sportcar;
import Lesson_28_Package.Truck;

import java.sql.SQLOutput;

public class Car {
    public static void main(String[] args){
        Cars car1 = new Cars("Mercedes", "Estate", 2.2, 100);
        System.out.println(car1.speed(105));
        System.out.println(car1.kmTransferToMile(150));
        System.out.println(car1.mileTransferToKM(100));
        System.out.println(car1.howFarYouCanDrive(30));


        Sportcar sportcar1 = new Sportcar("Lamborghini", "Aventador", 17.2, 90, 349, 700, 300000);
        System.out.println(sportcar1.howFarYouCanDrive(100));
        System.out.println(sportcar1.howFastWillOvercomeTheDistance(100));


        Truck truck1 = new Truck("Belaz", "75600", 20, 4360, 320000);
        System.out.println(truck1.howMuchLoaded(5001));
    }
}
