public class Homework {
    public static void main(String[] args) {
        //zada4a 1
        float sum = 0;
        float hundred = 100;
        for (float i = 0; i <= hundred; i++){
            sum = sum + i;
        }
        System.out.println(sum / 100);
//----------------------------------------------------------------------------------------------------------------------
        //zada4a 2
        int [] array = new int [10];
        array[0] = 15;
        array[1] = 24;
        array[2] = 48;
        array[3] = 92;
        array[4] = 3;
        array[5] = 1;
        array[6] = 35;
        array[7] = 5;
        array[8] = 63;
        array[9] = 382;

        //zada4a 2.1
        int[] array1 = new int [10];
        for (int i = 0; i < array1.length; i++){
            array1[i] = i;
            System.out.println("Array1 = " + array1[i]);
        }
//----------------------------------------------------------------------------------------------------------------------
        //zada4a 3
        int largestNumber = 0;
        int [] array3 = {5,2,4,8,88,22,10};
        for(int i = 0; i < array3.length; i++){
            if(array3[i] > largestNumber){
                largestNumber = array3[i];
            }
        }
        System.out.println(largestNumber);
    }
}
