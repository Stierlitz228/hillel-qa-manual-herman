import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class Main {
    WebDriver driver;

    @BeforeClass
    public static void mainPreconditions(){
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\WebDriver\\chromedriver.exe");
    }
    @Before
    public void precondition(){
        driver = new ChromeDriver();
    }
    @Test
    public void firstTest() throws InterruptedException {
        driver.get("https://user-data.hillel.it/html/registration.html");
        driver.findElement(By.cssSelector(".registration")).click();
        driver.findElement(By.id("first_name")).sendKeys("Herman", Keys.TAB);
        driver.findElement(By.id("last_name")).sendKeys("Danylch", Keys.TAB);
        driver.findElement(By.id("field_work_phone")).sendKeys("7993119", Keys.TAB);
        driver.findElement(By.id("field_phone")).sendKeys("380969971119", Keys.TAB);
        driver.findElement(By.id("field_email")).sendKeys("hermann855@gmail.com", Keys.TAB);
        driver.findElement(By.id("field_password")).sendKeys("Qwerty123", Keys.TAB);
        driver.findElement(By.id("male")).click();
        List<WebElement> option = driver.findElements(By.tagName("option"));
        option.get(2).click();
        driver.findElement(By.id("button_account")).click();
        Alert alert = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.alertIsPresent());
        alert.accept();
    }

    @Test
    public void secondTest(){
        driver.get("https://user-data.hillel.it/html/registration.html");
        driver.findElement(By.id("email")).sendKeys("hermann855@gmail.com", Keys.TAB);
        driver.findElement(By.id("password")).sendKeys("Qwerty123", Keys.TAB);
        driver.findElement(By.className("login_button")).click();
        driver.get("https://user-data.hillel.it/index.html");
        driver.findElement(By.id("info")).click();
    }

    @After
    public void postcondition() throws InterruptedException {
        Thread.sleep(5000);
        driver.quit();
    }

}
